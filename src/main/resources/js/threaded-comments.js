function RearrangeComments() {
    var issueID = JIRA.Issue.getIssueId();
    var parents = {};
    AJS.$.getJSON(AJS.contextPath() + "/rest/handlecomments/latest/hdata/commentdata?issueid=" + issueID, function (data) {
        AJS.$.each(data, function () {
            debug(this.commentid);
            parents[this.commentid] = this.parentcommentid;
        });
        AJS.$('div[id|=comment][id!=comment-wiki-edit]').each(function () {
            var commentId = AJS.$(this).attr('id').split('-')[1];

            var parent = parents[commentId];
            debug(commentId);
            if (parent) {
                var parentId = '#comment-' + parent;
                if (AJS.$(parentId).length != 0) {
                    debug("found parent in dom");
                    AJS.$(this).addClass('movedcomment');
                    AJS.$(this).appendTo(parentId);
                }
            }
            else {
                debug("no parent found for " + commentId);
            }
        });
	AJS.$('.issue-data-block.focused')
            .scrollIntoView({marginBottom: 200,marginTop: 200});
    });
}

function AddCommentButtons() {
    var loggedInUser = AJS.Meta.get('remote-user');
    var issueID = JIRA.Issue.getIssueId();
    var issueKey = AJS.Meta.get('issue-key');

    debug("AddCommentButtons called - " + issueID);

    AJS.$.getJSON(AJS.contextPath() + "/rest/api/latest/issue/" + issueKey, function (data) {
        var projectKey = data.fields.project.key;
        AJS.$('div[id|=comment][id!=comment-wiki-edit]').each(function () {
            var commentWholeId = AJS.$(this).attr('id');
            var commentId = AJS.$(this).attr('id').split('-')[1];

            var commentBlock = AJS.$(this).children()[0];
            if (AJS.$(commentBlock).find('.commentreply').length == 0) {
                debug("Adding Reply block and handler for commentId - " + commentId);
                AJS.$(commentBlock).append(AJS.$('<a class="aui-theme-default commentreply" href="#">Reply</a>'));
                AJS.$(commentBlock).append(AJS.$('<div class="commentreplyarea">' +
                    '<textarea class="textcommentreply textarea long-field wiki-textfield mentionable" cols="60" rows="10" wrap="virtual" ' +
                    'data-projectkey="' + projectKey + '" data-issuekey="' + issueKey + '" style="overflow-y: auto; height: 200px;"></textarea>' +
                    '<ul class="ops commentops">' +
                    '<li><a data="' + commentId + '" class="aui-button replycommentbutton">Add</a></li>' +
                    '<li><a href="#" data="' + commentId + '" class="aui-button aui-button-link cancel replycommentcancel">Cancel</a></li>' +
                    '<span class="icon throbber loading hiddenthrobber"></span>' +
                    '</ul>' +
                    '</div><br/>'));

                AJS.$(this).find('.commentreply').click(function (event) {
                    event.preventDefault();
                    AJS.$('.commentreplyarea').hide();
                    AJS.$('.commentreply').show();
                    AJS.$(this).next().toggle();
                    AJS.$(this).next().find('textarea').focus();
                    AJS.$(this).hide();
                });
                AJS.$(this).find('.replycommentbutton').click(function () {
                    var newComment = AJS.$(this).parent().parent().parent().children("textarea").val();
                    if (newComment.length == 0) {
                        debug("empty input");
                        return;
                    }
                    AJS.$(this).find('.hiddenthrobber').show();
                    var encoded = AJS.$('<div/>').text(newComment).html();
                    var postData = {};
                    postData.commentbody = newComment;
                    postData.parentcommentid = AJS.$(this).attr('data');
                    postData.issueid = issueID;

                    debug("new data " + postData);
                    AJS.$.ajax({
                        url: AJS.contextPath() + "/rest/handlecomments/latest/hdata/addcomment",
                        data: JSON.stringify(postData),
                        type: "POST",
                        contentType: "application/json",
                        success: function (data) {
                            debug("New comment added :" + data);
                            JIRA.trigger(JIRA.Events.REFRESH_ISSUE_PAGE, [JIRA.Issue.getIssueId(), {
                                complete: function () {
                                    AJS.$("#comment-" + data.commentid).scrollIntoView({marginBottom: 200, marginTop: 200});
                                    AJS.$('div[id|=comment][id!=comment-wiki-edit]').removeClass('focused');
                                    AJS.$("#comment-" + data.commentid).addClass('focused');
                                }
                            }]);
                            AJS.$(this).find('.hiddenthrobber').hide();
                        }
                    });
                });
                AJS.$(this).find('.replycommentcancel').click(function (event) {
                    event.preventDefault();
                    AJS.$(this).parent().parent().parent().toggle();
                    AJS.$(this).closest('.issue-data-block').find('.commentreply').show();
                });
            }
        });
    });

}

AJS.$('document').ready(function () {
    AddCommentButtons();
    RearrangeComments();
    JIRA.ViewIssueTabs.onTabReady(function () {
        AddCommentButtons();
        RearrangeComments();
    });
    JIRA.bind(JIRA.Events.REFRESH_ISSUE_PAGE, function (e, context, reason) {
        RearrangeComments();
        AddCommentButtons();
    });
    JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function (e, context, reason) {
        var isDeletePage = AJS.$('#comment-delete-submit');
        if (isDeletePage) {
            AJS.$('#comment-delete-submit').click(function () {
                  AJS.$.getJSON(AJS.contextPath() + "/rest/handlecomments/latest/hdata/deletecomment?commentid=" + AJS.$('input[name="commentId"]').attr('value'), function (data) {
                      debug(data);
                });
            });
        }
    });
});

function debug(message) {
    //console.log(message);
}